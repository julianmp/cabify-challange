import { products } from '../../src/data/data.json';

describe('Product info modal', () => {
  const Shirt = products[0];
  const Mug = products[1];

  it('should render when proudct is clicked', () => {
    cy.viewport(1440, 769).visit('http://localhost:5173');

    cy.get(`main[data-testid=modal]`).should('not.exist');
    cy.get(`#${Mug.id}_quantity`).should('have.value', 0);
    cy.get(`[data-testid=product-${Mug.id}]`).click();
    cy.get(`main[data-testid=modal]`).should('exist');

    cy.get('#product_name').should('have.text', Mug.name);
    cy.get('#product_description').should('have.text', Mug.description);
    cy.get('#product_code').should('have.text', Mug.id);

    cy.get('#product_add_button').click();
    cy.get(`main[data-testid=modal]`).should('not.exist');

    cy.get(`#${Mug.id}_quantity`).should('have.value', 1);
  });
});
