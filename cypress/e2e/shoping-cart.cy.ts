/// <reference types="cypress" />
import { products } from '../../src/data/data.json';

describe('Shopping cart challange', () => {
  const Shirt = products[0];
  const Mug = products[1];
  const Cap = products[2];

  beforeEach('Currently not responsive', () => {
    cy.viewport(1440, 769);
  });

  it('should start with empty cart', () => {
    cy.visit('http://localhost:5173');

    cy.get(`#${Shirt.id}_quantity`).should('have.value', 0);
    cy.get(`#${Mug.id}_quantity`).should('have.value', 0);
    cy.get(`#${Cap.id}_quantity`).should('have.value', 0);

    cy.get(`#${Shirt.id}_discount`).should('have.text', '- 0 €');
    cy.get(`#${Mug.id}_discount`).should('have.text', '- 0 €');

    cy.get(`#checkout_total`).should('have.text', '0 €');
    cy.get(`#checkout_quantity`).should('have.text', '0 Items');

    cy.get(`#${Shirt.id}_add`).click();
    cy.get(`#${Shirt.id}_quantity`).should('have.value', 1);

    cy.get(`#${Shirt.id}_substract`).click();
    cy.get(`#${Shirt.id}_quantity`).should('have.value', 0);
  });

  it('shoping list example', () => {
    cy.visit('http://localhost:5173');

    cy.intercept('**/?X7R2OPX_quantity=3&X2G2OPZ_quantity=2&X3W2OPY_quantity=0').as('form-submit');

    cy.get(`#${Shirt.id}_add`).click().click().click();
    cy.get(`#${Mug.id}_add`).click().click();

    cy.get(`#${Shirt.id}_quantity`).should('have.value', 3);
    cy.get(`#${Mug.id}_quantity`).should('have.value', 2);
    cy.get(`#${Cap.id}_quantity`).should('have.value', 0);

    cy.get(`#${Shirt.id}_discount`).should('have.text', '- 3 €');
    cy.get(`#${Mug.id}_discount`).should('have.text', '- 5 €');

    cy.get(`#checkout_total`).should('have.text', '70 €');
    cy.get('#checkout_finalTotal').should('have.text', '62 €');
    cy.get(`#checkout_quantity`).should('have.text', '5 Items');

    cy.get('.summary-total > button').click();

    cy.wait('@form-submit');
  });

  it('loads cart from query params', () => {
    cy.visit('http://localhost:5173/?X7R2OPX_quantity=3&X2G2OPZ_quantity=2&X3W2OPY_quantity=0');

    cy.get(`#${Shirt.id}_quantity`).should('have.value', 3);
    cy.get(`#${Mug.id}_quantity`).should('have.value', 2);
    cy.get(`#${Cap.id}_quantity`).should('have.value', 0);
  });
});
