import { Discount, ICheckout, Product } from './types';

export class Checkout implements ICheckout {
  // A map of product codes to quantities in the cart
  public cart: { [code: string]: number } = {};
  // A map of product codes to product objects
  private products: { [code: string]: Product } = {};

  constructor(products: Product[]) {
    // Add each product to the `products` map
    for (const product of products) {
      this.products[product.id] = { ...product };
    }
  }

  /**
   * Returns the price of the product with the given code.
   * @param code The product code
   */
  private getPriceForProduct(code: string): number {
    // Look up the product in the `products` map
    const product = this.products[code];
    // If the product doesn't exist, return 0
    if (!product) return 0;
    // Return the product's price
    return product.price;
  }

  /**
   * Scans a product and adds it to the current cart.
   *
   * @param code The product identifier
   * @param quantity Fixed quantity to set for the given product (optional). This can be usefull to remove items.
   * @returns itself to allow function chaining
   */
  scan(code: string, quantity?: number): this {
    // If product don't exist in the cart is set to 0
    if (this.cart[code] === undefined) {
      this.cart[code] = 0;
    }

    // If `quantiy` is not set, increments product quantity by 1
    // Otherways set it to `quantity`
    if (quantity === undefined) {
      this.cart[code] += 1;
    } else {
      this.cart[code] = quantity;
    }

    // Return `this` to allow function chaining
    return this;
  }

  /**
   * Returns the value of all cart products with the discounts applied.
   */
  total(): number {
    // Initialize the total to 0
    let total = 0;
    // Iterate over the product codes in the cart
    for (const code in this.cart) {
      // Look up the price of the product
      const price = this.getPriceForProduct(code);
      // Get the quantity of the product in the cart
      const quantity = this.cart[code];
      // Calculate the regular total for this product
      const regularTotal = quantity * price;
      // Add the regular total to the overall total
      total += regularTotal;
    }
    // Get the discounts to apply
    const discounts = this.getDiscounts();
    // Iterate over the discounts
    for (const discount of discounts) {
      // Subtract the discount amount from the overall total
      total -= discount.discount;
    }
    // Return the final total
    return total;
  }

  /**
   * Returns a list of discounts to apply to the cart.
   *
   * This can be outside class to allow making changes in discounts without changing Checkout implementation.
   * As is not part of requirements, decided to add discounts here to save time.
   */
  getDiscounts(): Discount[] {
    const discounts: Discount[] = [];
    for (const code in this.cart) {
      const quantity = this.cart[code];
      // Mug 2x1 offer
      if (code === 'X2G2OPZ' && quantity >= 2) {
        // Calculate the discount amount
        const discount = ((quantity - (quantity % 2)) / 2) * this.getPriceForProduct(code);
        // Add the discount to the array of discounts
        discounts.push({ code, discount });
      }
      // Shirt offer
      if (code === 'X7R2OPX' && quantity >= 3) {
        // Calculate the discount amount
        const discount = quantity * this.getPriceForProduct(code) * 0.05;
        // Add the discount to the array of discounts
        discounts.push({ code, discount });
      }
    }
    return discounts;
  }
}

export default Checkout;
