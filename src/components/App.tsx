import React, { useMemo } from 'react';
import { getCheckout, useCheckout } from '../getCheckout';
import { ProductRow } from './ProductRow';
import { useProductModal } from './ProductModal';

const { checkout, products } = getCheckout();

/**
 * App component [Main]
 */
export const App: React.FC = () => {
  // Get the state and functions from the useCheckout hook
  const { cart, total, discounts, scan } = useCheckout(checkout);

  // Calculate the subtotal by adding the total and all discounts
  const subTotal = useMemo(() => total + discounts.reduce((acc, d) => acc + d.discount, 0), [total, discounts]);
  // Calculate the total number of items in the cart
  const items = useMemo(() => Object.keys(cart).reduce((acc, p) => acc + cart[p], 0), [total, discounts]);

  // Modal component and a function to open it
  const [ModalProducto, openModal] = useProductModal((id) => scan(id));

  return (
    <>
      <main className="App">
        <section className="products">
          <h1 className="main">Shopping cart</h1>
          <ul className="products-list tableHead">
            <li className="products-list-title row">
              <div className="col-product">Product details</div>
              <div className="col-quantity">Quantity</div>
              <div className="col-price">Price</div>
              <div className="col-total">Total</div>
            </li>
          </ul>
          <form id="product_list">
            <ul className="products-list">
              {products.map((product) => (
                <ProductRow
                  {...product}
                  quantity={cart[product.id] || 0}
                  onChange={(q) => scan(product.id, q)}
                  onClick={() => openModal(product)}
                  key={product.id}
                />
              ))}
            </ul>
          </form>
        </section>
        <aside className="summary">
          <h1 className="main">Order Summary</h1>
          <ul className="summary-items wrapper border">
            <li>
              <span className="summary-items-number" id="checkout_quantity">
                {items} Items
              </span>
              <span className="summary-items-price" id="checkout_total">
                {subTotal} €
              </span>
            </li>
          </ul>
          <div className="summary-discounts wrapper-half border">
            <h2>Discounts</h2>
            <ul>
              <li>
                <span>x3 Shirt offer</span>
                <span id="X7R2OPX_discount">- {discounts.find((d) => d.code === 'X7R2OPX')?.discount || 0} €</span>
              </li>
              <li>
                <span>2x1 Mug offer</span>
                <span id="X2G2OPZ_discount">- {discounts.find((d) => d.code === 'X2G2OPZ')?.discount || 0} €</span>
              </li>
            </ul>
          </div>
          <div className="summary-total wrapper">
            <ul>
              <li>
                <span className="summary-total-cost">Total cost</span>
                <span className="summary-total-price" id="checkout_finalTotal">
                  {total} €
                </span>
              </li>
            </ul>
            <button type="submit" form="product_list">
              Checkout
            </button>
          </div>
        </aside>
      </main>
      {ModalProducto}
    </>
  );
};
