import '@testing-library/jest-dom/extend-expect';
import { render, fireEvent } from '@testing-library/react';
import { ProductRow } from './ProductRow';
import { products } from '../data/data.json';
import { vi } from 'vitest';

describe('ProductRow', () => {
  const product = products[0];

  it('displays the product details and quantity', () => {
    const quantity = 2;
    const onChange = vi.fn();
    const onClick = vi.fn();

    const { getByTestId, getByLabelText } = render(<ProductRow {...product} quantity={quantity} onChange={onChange} onClick={onClick} />);

    // Check that the product details are displayed
    expect(getByTestId(`product-${product.id}`)).toBeInTheDocument();
    expect(getByLabelText('product_row')).toBeInTheDocument();
    expect(getByLabelText(`${product.name} Quantity`)).toHaveValue(2);
  });

  it('clicks on + change quantity', () => {
    const quantity = 2;
    const onChange = vi.fn();
    const onClick = vi.fn();

    const { getByTestId, getByLabelText } = render(<ProductRow {...product} quantity={quantity} onChange={onChange} onClick={onClick} />);

    const addButton = getByTestId(`${product.id}_add`);

    expect(getByLabelText(`${product.name} Quantity`)).toHaveValue(2);
    addButton.click();

    expect(onChange).toHaveBeenCalledWith(3);
  });

  it('clicks on - change quantity', () => {
    const quantity = 2;
    const onChange = vi.fn();
    const onClick = vi.fn();

    const { getByTestId, getByLabelText } = render(<ProductRow {...product} quantity={quantity} onChange={onChange} onClick={onClick} />);

    const substractButton = getByTestId(`${product.id}_substract`);

    expect(getByLabelText(`${product.name} Quantity`)).toHaveValue(2);
    substractButton.click();

    expect(onChange).toHaveBeenCalledWith(1);
  });

  it('calls the onChange function when the quantity is changed', () => {
    const quantity = 2;
    const onChange = vi.fn();
    const onClick = vi.fn();

    const { getByLabelText } = render(<ProductRow {...product} quantity={quantity} onChange={onChange} onClick={onClick} />);

    const input = getByLabelText(`${product.name} Quantity`);
    fireEvent.change(input, { target: { value: '3' } });
    expect(onChange).toHaveBeenCalledWith(3);
  });

  it('calls the onClick function when the product row is clicked', () => {
    const quantity = 2;
    const onChange = vi.fn();
    const onClick = vi.fn();

    const { getByTestId } = render(<ProductRow {...product} quantity={quantity} onChange={onChange} onClick={onClick} />);

    const row = getByTestId(`product-${product.id}`);
    fireEvent.click(row);
    expect(onClick).toHaveBeenCalled();
  });

  it('disables the substract button when quantity is 0', () => {
    const quantity = 0;
    const onChange = vi.fn();
    const onClick = vi.fn();

    const { getByTestId } = render(<ProductRow {...product} quantity={quantity} onChange={onChange} onClick={onClick} />);

    const substractButton = getByTestId(`${product.id}_substract`);
    expect(substractButton).toBeDisabled();
  });
});
