import React from 'react';
import { Product } from '../types';

type ProductRowProps = Product & {
  quantity: number;
  onChange: (quantity: number) => void;
  onClick: () => void;
};

// Functional component to display a row for each product in cart
export const ProductRow: React.FC<ProductRowProps> = ({ quantity, id, name, price, onChange, onClick }) => {
  const totalPrice = price * quantity;

  const setQuantity = (value: string) => {
    const quantity = value === undefined ? 0 : Number(value);
    onChange(quantity);
  };

  return (
    <>
      <li className="product row" aria-label="product_row">
        <div data-testid={`product-${id}`} className="col-product" onClick={onClick}>
          <figure className="product-image">
            <img id={`${id}_img`} src={`${id}_thumb.png`} alt="Shirt" />
            <div className="product-description">
              <h1>{name}</h1>
              <p className="product-code">Product code ${id}</p>
            </div>
          </figure>
        </div>
        <div className="col-quantity">
          <button
            type="button"
            className="count"
            id={`${id}_substract`}
            data-testid={`${id}_substract`}
            disabled={quantity === 0}
            onClick={() => onChange(quantity - 1)}
          >
            -
          </button>
          <input
            type="number"
            className="product-quantity"
            name={`${id}_quantity`}
            id={`${id}_quantity`}
            min="0"
            value={quantity}
            aria-label={`${name} Quantity`}
            onChange={(e) => setQuantity(e.target.value)}
          />
          <button type="button" className="count" id={`${id}_add`} data-testid={`${id}_add`} onClick={() => onChange(quantity + 1)}>
            +
          </button>
        </div>
        <div className="col-price">
          <span className="product-price" id={`${id}_unit`}>
            {price} €
          </span>
        </div>
        <div className="col-total">
          <span className="product-price" id={`${id}_total`}>
            {totalPrice} €
          </span>
        </div>
      </li>
    </>
  );
};
