import React, { useState } from 'react';
import { Product } from '../types';

// Functional component to display modal for a product
export const ProductModal: React.FC<Product & { handleAddToCart: () => void; handleClose: () => void }> = (props) => {
  const { id, name, price, description, handleAddToCart, handleClose } = props;

  const imageSrc = `/${id}.jpg`;

  return (
    <>
      <main data-testid="modal" className="Modal">
        <section className="modal_image">
          <img src={imageSrc} id="product_image" alt="Product image"></img>
        </section>
        <aside className="modal_summary">
          <span id="modal_close" className="close" onClick={handleClose}>
            &times;
          </span>
          <div className="content">
            <h1 className="main">
              <span id="product_name">{name}</span>
              <span id="product_price">{price} €</span>
            </h1>
            <p id="product_description">{description}</p>
            <br></br>
            <div>
              Product code <span id="product_code">{id}</span>
            </div>
            <button id="product_add_button" onClick={handleAddToCart}>
              Add to cart
            </button>
          </div>
        </aside>
      </main>
    </>
  );
};

// Custom hook to manage product modal state and functions
export const useProductModal = (scan: (code: string) => void): [JSX.Element, (product: Product) => void] => {
  // State for current product in modal
  const [product, setProduct] = useState<Product>();

  // Function to open modal for a product
  const openModal = (product: Product) => setProduct(product);

  // Function to close modal
  const closeModal = () => {
    setProduct(undefined);
  };

  // Function to handle adding product to cart
  const handleAddToCart = () => {
    scan(product!.id);
    closeModal();
  };

  // Render modal if product is defined, else render empty element
  const modal = product ? <ProductModal {...product} handleAddToCart={handleAddToCart} handleClose={closeModal} /> : <></>;

  return [modal, openModal];
};
