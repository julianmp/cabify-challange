import '@testing-library/jest-dom';
import { render, fireEvent } from '@testing-library/react';
import { App } from './App';

describe('App', () => {
  it('should render the shopping cart and order summary sections', () => {
    const { getByText } = render(<App />);
    expect(getByText('Shopping cart')).toBeInTheDocument();
    expect(getByText('Order Summary')).toBeInTheDocument();
  });

  it('should render a list of products and their details', () => {
    const { getByText } = render(<App />);
    expect(getByText('Product details')).toBeInTheDocument();
    expect(getByText('Quantity')).toBeInTheDocument();
    expect(getByText('Price')).toBeInTheDocument();
    expect(getByText('Total')).toBeInTheDocument();
  });

  it('should open the product modal when a product is clicked', () => {
    const { getByTestId } = render(<App />);
    fireEvent.click(getByTestId('product-X2G2OPZ'));
    expect(getByTestId('modal')).toBeInTheDocument();
  });
});
