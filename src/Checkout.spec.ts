import { Checkout } from './Checkout';
import data from './data/data.json';

describe('Checkout', () => {
  let checkout: Checkout;

  beforeEach(() => {
    // Create a new Checkout instance before each test
    checkout = new Checkout(data.products);
  });

  test('scanning a product adds it to the cart', () => {
    checkout.scan('X7R2OPX');
    expect(checkout.total()).toBe(20);
  });

  test('scanning multiple copies of the same product adds the correct quantity to the cart', () => {
    checkout.scan('X7R2OPX').scan('X7R2OPX').scan('X2G2OPZ');
    expect(checkout.total()).toBe(45);
  });

  test('scanning multiple different products adds all of them to the cart', () => {
    checkout.scan('X7R2OPX').scan('X2G2OPZ').scan('X3W2OPY');
    expect(checkout.total()).toBe(35);
  });

  test('should apply discounts for MUG items', () => {
    // Scan 2 MUG items
    checkout.scan('X2G2OPZ').scan('X2G2OPZ');

    const discounts = checkout.getDiscounts();
    expect(discounts).toHaveLength(1);
    // Expect the discount amount to be 5 (1 free MUG)
    expect(discounts[0].discount).toBe(5);
  });

  test('should apply discounts for TSHIRT items', () => {
    // Scan 3 TSHIRT items
    checkout.scan('X7R2OPX', 3);

    const discounts = checkout.getDiscounts();
    expect(discounts).toHaveLength(1);
    // Expect the discount amount to be 3 (3 * 20 * 0.05)
    expect(discounts[0].discount).toBe(3);
  });
});
