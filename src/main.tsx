import './styles/reset.css';
import './styles/main.css';

import * as React from 'react';
import * as ReactDOM from 'react-dom/client';
import { App } from './components/App';

const root = ReactDOM.createRoot(document.getElementById('root')!);
const component = React.createElement(App);

// Migrated main UI to React components. Allowed me to make code more clear and reactive.
root.render(component);
