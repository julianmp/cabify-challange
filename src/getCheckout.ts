import { useEffect, useState } from 'react';
import { Checkout } from './Checkout';
import data from './data/data.json';

export function getCheckout() {
  return { checkout: new Checkout(data.products), products: data.products };
}

// Custom hook that receives an instance of the Checkout class and returns the
// current total of the purchase, as well as the function to scan a product
export const useCheckout = (checkout: Checkout) => {
  // Effect to load initial quantities from queryparams
  useEffect(() => {
    const params = new URLSearchParams(window.location.search);
    data.products.forEach(({ id }) => {
      const param = params.get(`${id}_quantity`);
      const quantity = Number(param) || 0;
      scan(id, quantity);
    });
  }, []);

  // State that stores the current total of the purchase.
  const [total, setTotal] = useState(checkout.total());
  const [cart, setCart] = useState(checkout.cart);
  // State that stores the current discounts of the purchase.
  const [discounts, setDiscounts] = useState(checkout.getDiscounts());

  // Function to scan a product. Keeps total updated.
  const scan = (code: string, count?: number) => {
    checkout.scan(code, count);

    setCart(checkout.cart);
    setTotal(checkout.total());
    setDiscounts(checkout.getDiscounts());
  };

  return { cart, total, discounts, scan };
};
