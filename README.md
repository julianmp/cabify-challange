# Cabify Challenge

Hi! welcome to Cabify's frontend challenge. First and foremost, **read this document carefully**, as we want to make sure all the implementation details are well understood. As you will see in the explanation, there are some things that we **require you** to implement to consider this **exercise valid**. If you have any doubts after reading it, don't hesitate to ask your go-to person.

## The Challenge

As you already know, besides providing exceptional transportation services, Cabify also runs a physical store that sells 3 products for now. Our current stock consists of the following products:

```
Code         | Name                |  Price
-------------------------------------------------
TSHIRT       | Cabify T-Shirt      |  20.00€
MUG          | Cabify Coffee Mug   |   5.00€
CAP          | Cabify Cap          |  10.00€
```

We allow the users the possibility of having some discounts applied when combining the products in the following ways:

- 2-for-1 promotions: (for `MUG` items). Buy two of them, get one free. (e.g: pay 10€ for 4 mugs)
- Bulk discounts: (for `TSHIRT` items). Buying 3 or more of this product, the price per unit is reduced by 5%. (e.g: if you buy 3 or more `TSHIRT` items, the price per unit should be 19.00€)

## The Requirements

### Functional requirements

1. The user should be able to add and remove products from the cart
1. The user should be able to see the current state of the cart (number of items, discounts, etc.) as they're adding/removing items
1. The total amount (with discounts applied) should be calculated when the user pushes the `Checkout` button
1. A modal should open when the user clicks on each product in the cart. This modal should contain the details of the clicked item. You'll find here the [UI design](https://www.figma.com/file/FnGUlbUqeUOKvk6xgdCMvo/Shopping-cart-challenge) and all the assets you will need inside `/public` folder.

### Technical requirements

1. Create a **Checkout class** that **can** be instantiated. Make sure you include this inside the `src/Checkout.(js|ts)` file. This class should implement the following interface (if you don't know what a Typescript interface is just [check this out](https://www.typescriptlang.org/docs/handbook/2/objects.html)).

   ```typescript
   interface Checkout {
     /**
      * Scans a product adding it to the current cart.
      * @param code The product identifier
      * @returns itself to allow function chaining
      */
     scan(code: string): this;
     /**
      * Returns the value of all cart products with the discounts applied.
      */
     total(): number;
   }
   ```

   An instance of this class should be able to add products via the `scan` method and, using the `total` method, obtain as a `number` the total price of the items in the cart **with the discounts already applied**.

   E.g:

   ```javascript
   const co = new Checkout(pricingRules);
   co.scan('TSHIRT').scan('CAP').scan('TSHIRT');
   const totalPrice = co.total();
   ```

## What do we want you to do?

Dont' worry, you don't have to do all the above from scratch. We've provided you with the markup (HTML), a [basic implementation of the styles](./example.png) and some vanilla _Javascript_ to interact with the view. Please, focus on the following:

1. `Checkout` class solution.
1. Integrating `Checkout` class into the _User interface_.
1. Making the detail modal (from scratch)

## FAQ

### Besides the Checkout class, can I add more classes?

Yes, you can add as many classes/modules as you want. Just remember that **`Checkout` class is mandatory**. We won't consider the challenge as valid if the `Checkout` class is missing.

### Which files should I modify?

You can change **anything you need** to fulfill the requirements. Create as many files and refactor as many code as you want, but bear in mind the restrictions in the [readiness checklist](#readiness-checklist)

### But I think I've seen some bugs or smells in the provided code... can I fix them?

Of course! again, feel free to refactor and change all you think it could be improved. You can also add any additional library you need. Just bear in mind the restrictions in the [readiness checklist](#readiness-checklist)

### I really need to change something you told me not to change... may I?

If you are very convinced to change any of the requirements for a good reason, you can do it. But please, **justify all your decisions**. Feel free to add comments, extra documentation, links, etc.

### Should I add automatic tests?

It's not mandatory, but we encourage you to add them (This challenge already support creating unit test with _Vitest_). It's a great way of ensuring your solution works properly and showing us your seniority.

### Can I add documentation?

Definitely YES. We know technical decisions are subjective and can be discussed, so explain and justify all your decissions, trade-offs, and everything you could think is relevant to understand your solution.

### Can I use Typescript?

Yes, we love Typescript. If you don't know it, don't worry. It's not mandatory. If you do, just change the extension of your files to `.ts`.

### I still have questions, what can I do?

Feel free to ask anything you have questions about, and act as a product owner.

## Development

### Set up

Requisites:

- node@16.17.0 (check this out in `.nvmrc` file, package.json#engine.node or package.json#volta.node).
- npm@8.18.0 (check this out in package.json#volta.node).

To run the solution just:

- Run `npm install`.
- Run `npm run dev`.

To build the solution just:

- Run `npm run build`. It will create a bundle under `/dist` directory.

To run this solution we decided to choose a modern and simple bundler called _vite_ (documentation [here](https://vitejs.dev/)), which provides us with an easy development setup with _Javascript_ and _Typescript_.
**In case you want** to include unit testing we created basic configuration with [vitest](https://vitest.dev/guide/) to support Javascript testing. If you are familiar with Jest, it has the same exact implementation! (remember unit testing is not required to complete the challenge).

- Create a file with `*.spec|test.js|ts` extension.
- Run the test with `npm test`.

### Readiness checklist

Before submitting the challenge, check the next bullet points:

- [x] The code must build and execute on a Unix platform.
- [x] The code must be submitted in _Gitlab_ repo, in _master_ branch, **and avoid using a different branch**. Only _master_ branch should remain.
- [x] Pay attention to `Checkout` class requirements, we will discard the challenge in case some requirement is missing.
- [x] You haven't renamed/removed any of the given files.
- [x] You haven't modified the HTML nodes, we will need `ids` and `aria_labels` to perform some automatic tests.
- [x] You haven't modified the default _vite_ development server port (default post 5173).
- [x] You haven't modified the default _vite_ build output directory (default directory _dist_).
- [x] You haven't modified the `.gitlab-ci.yml` file.
- [x] All Gitlab pipelines must pass as Ok ✅.
- [x] We appreciate Feedback, can you [fill this form](https://docs.google.com/forms/d/e/1FAIpQLSf2Yykjcp2UryGPSIfNVoKZCV1LDIjBs0G4JZFZaVEbkAb1Nw/viewform) in order for us to improve the experience of completing this Challenge.
